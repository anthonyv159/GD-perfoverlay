extends VBoxContainer

@export var background_color: Color= Color(0.0, 0.0, 0.0, 0.5)
@export var plot_graphs: bool= true
@export var graph_color:= Color.ORANGE
@export var normalize_units:= true
@export var history:= 100
@export var graph_height:= 50


@export var fps: int= 1 #0=False, 1=Text Only, 2=Graph
@export var process := false

#Game
@export var Game_ver: String=str(ProjectSettings.get_setting("application/config/version"))

# OS and Hardware Stuff
var OS_name: String
var Engine_name: String
@onready var GPU_adapter = RenderingServer.get_video_adapter_name()

@export var graphics_driver: String= str()
@export var cpu_arch: bool= false

# Memory
@export var physics_process: bool= false
@export var static_memory: bool= false
@export var max_static_memory: bool= false
@export var max_message_buffer: bool= false

#Rendering
@export var objects: bool= false
@export var resources: bool= false
@export var nodes: bool= false
@export var orphan_nodes: bool= false

#GPU
@export var video_memory: bool= false
@export var texture_memory: bool= false

#Phyisics
@export var active_objects_2d: bool= false
@export var collision_pairs_2d: bool= false
@export var islands_2d: bool= false
@export var active_objects_3d: bool= false
@export var collision_pairs_3d: bool= false
@export var islands_3d: bool= false

#Audio
@export var audio_output_latency: bool= false


var _timer: Timer
var _font: FontFile = load("res://addons/monitor_overlay/font/overlay_font.tres")
var _debug_graph = load("res://addons/monitor_overlay/monitor_overlay_debug_graph.gd")
var _graphs := []


func _ready():
	DisplayServer.window_set_title(ProjectSettings.get_setting("application/config/name")  + " using Godot v." + Engine.get_version_info()["string"] + " Content v." + str(Game_ver) + " - " + graphics_driver)

	if custom_minimum_size.x == 0:
		custom_minimum_size.x = 300
	rebuild_ui()


func clear() -> void:
	for graph in _graphs:
		graph.queue_free()
	_graphs = []


func rebuild_ui() -> void:
	clear()

	# FPS
	match fps:
		1:
			_create_text_for(str(Performance.TIME_FPS)+"/", "FPS")
		2:
			_create_graph_for(Performance.TIME_FPS, "FPS")

	if process:
		_create_graph_for(Performance.TIME_PROCESS, "Process", "s")
	
	if physics_process:
		_create_graph_for(Performance.TIME_PHYSICS_PROCESS, "Physics Process", "s")
	
	if graphics_driver:
		match OS.RenderingDriver:
			OS.RENDERING_DRIVER_VULKAN:
				match OS_name:
					"Android", "iOS", "OSX", "Windows", "UWP", "X11":
						_create_custom_text_for("Vulkan", "Graphics Driver")
					_:
						_create_custom_text_for("Error?", "Graphics Driver")
			OS.RENDERING_DRIVER_OPENGL3:
				match OS_name:
					"Android", "iOS":
						_create_custom_text_for("OpenGL ES 3.0", "Graphics Driver")
					"OSX", "Windows", "UWP", "X11":
						_create_custom_text_for("OpenGL 3.3", "Graphics Driver")
					"HTML5":
						_create_custom_text_for("WebGL 2.0", "Graphics Driver")
			_:
				_create_custom_text_for("Headless", "Graphics Driver")

	if static_memory:
		_create_graph_for(Performance.MEMORY_STATIC, "Static Memory", "B")

	if max_static_memory:
		_create_graph_for(Performance.MEMORY_STATIC_MAX, "Max Static Memory", "B")

	if max_message_buffer:
		_create_graph_for(Performance.MEMORY_MESSAGE_BUFFER_MAX, "Message Buffer Max")

	if objects:
		_create_graph_for(Performance.OBJECT_COUNT, "Objects")

	if resources:
		_create_graph_for(Performance.OBJECT_RESOURCE_COUNT, "Resources")

	if nodes:
		_create_graph_for(Performance.OBJECT_NODE_COUNT, "Nodes")

	if orphan_nodes:
		_create_graph_for(Performance.OBJECT_ORPHAN_NODE_COUNT, "Orphan Nodes")


	if video_memory:
		_create_graph_for(Performance.RENDER_VIDEO_MEM_USED, "Video Memory", "B")

	if texture_memory:
		_create_graph_for(Performance.RENDER_TEXTURE_MEM_USED, "Texture Memory", "B")


	if active_objects_2d:
		_create_graph_for(Performance.PHYSICS_2D_ACTIVE_OBJECTS, "2D Active Objects")

	if collision_pairs_2d:
		_create_graph_for(Performance.PHYSICS_2D_COLLISION_PAIRS, " 2D Collision Pairs")

	if islands_2d:
		_create_graph_for(Performance.PHYSICS_2D_ISLAND_COUNT, "2D Islands")

	if active_objects_3d:
		_create_graph_for(Performance.PHYSICS_3D_ACTIVE_OBJECTS, " 3D Active Objects")

	if collision_pairs_3d:
		_create_graph_for(Performance.PHYSICS_3D_COLLISION_PAIRS, "3D Collision Pairs")

	if islands_3d:
		_create_graph_for(Performance.PHYSICS_3D_ISLAND_COUNT, "3D Islands")

	if audio_output_latency:
		_create_graph_for(Performance.AUDIO_OUTPUT_LATENCY, "Audio Latency", "s")


func _process(_delta) -> void:
	for item in _graphs:
		item.update()


func _create_graph_for(monitor: int, name: String, unit: String = "") -> void:
	var graph = _debug_graph.new()
	graph.monitor = monitor
	graph.monitor_name = name
	graph.font = _font
	graph.rect_min_size.y = graph_height
	graph.max_points = history
	graph.background_color = background_color
	graph.graph_color = graph_color
	graph.plot_graph = plot_graphs
	graph.unit = unit
	graph.normalize_units = normalize_units

	add_child(graph)
	_graphs.push_back(graph)

func _create_text_for(monitor: String, name: String, unit: String = "") -> void:
	var graph = _debug_graph.new()
	graph.monitor = monitor
	graph.monitor_name = name
	graph.font = _font
	graph.rect_min_size.y = 15
	graph.max_points = history
	graph.background_color = Color.TRANSPARENT
	graph.graph_color = Color.TRANSPARENT
	graph.plot_graph = plot_graphs
	graph.unit = unit
	graph.normalize_units = normalize_units

	add_child(graph)
	_graphs.push_back(graph)

func _create_custom_text_for(monitor, name: String, unit: String = "") -> void:
	var graph = _debug_graph.new()
	graph.monitor = monitor
	graph.monitor_name = name
	graph.font = _font
	graph.rect_min_size.y = 15
	graph.max_points = history
	graph.background_color = Color.TRANSPARENT
	graph.graph_color = Color.TRANSPARENT
	graph.plot_graph = plot_graphs
	graph.unit = unit
	graph.normalize_units = normalize_units

	add_child(graph)
	_graphs.push_back(graph)
